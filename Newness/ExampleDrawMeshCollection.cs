﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Vanilla.DrawMeshes
{
    [Serializable]
    public class ExampleDrawMeshInstancedSlot : DrawMeshInstancedSlot
    {
        public override void Initialize()
        {
            UpdateBuffers();
        }

        public override void UpdateBuffers()
        {
            
        }
    }

    [Serializable]
    public class ExampleDrawMeshCollection : DrawMeshCollection<ExampleDrawMeshInstancedSlot>
    {

    }
}
