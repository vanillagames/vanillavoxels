﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vanilla;
using Vanilla.Voxels;
using Random = UnityEngine.Random;

[Serializable]
public class MeshVoxel
{
    public static MeshMatrix matrix;
    
    public bool solid;
    
    public byte solidNeighbourState;
    public byte directionByte; // 0 => north 1 => south 2 => east 3 => west
    public byte type;
    public byte subtype;

    public MeshVoxel(int x, int y, int z)
    {
//        solid = Random.value < 0.5f;

        solid = y == 0;
    }

    public virtual void UpdateSolidState(int x, int y, int z)
    {
        solidNeighbourState = 0;

        if (NeighbourSolid(x: x + 1, y: y, z: z)) solidNeighbourState = 1;
        if (NeighbourSolid(x: x - 1, y: y, z: z)) solidNeighbourState |= 1 << 2;
        if (NeighbourSolid(x: x, y: y + 1, z: z)) solidNeighbourState |= 1 << 3;
        if (NeighbourSolid(x: x, y: y - 1, z: z)) solidNeighbourState |= 1 << 4;
        if (NeighbourSolid(x: x, y: y, z: z + 1)) solidNeighbourState |= 1 << 5;
        if (NeighbourSolid(x: x, y: y, z: z - 1)) solidNeighbourState |= 1 << 6;
        
        if (solid) solidNeighbourState |= 1 << 7;
        
        Debug.Log($"I determined my solidNeighbourState to be [{solidNeighbourState}]");
    }

    protected bool NeighbourSolid(int x, int y, int z)
    {
        return !matrix.InvalidPosition(x,y,z) && matrix.matrix[x, y, z].solid;
    }
}

[Serializable]
public class MeshMatrix : VanillaMatrix<MeshVoxel>
{
    protected override void InitializeNewMatrix()
    {
        for (var z = 0; z < zSize; z++)
        {
            for (var y = 0; y < ySize; y++)
            {
                for (var x = 0; x < xSize; x++)
                {
                    matrix[x,y,z] = new MeshVoxel(x,y,z);
                }
            }
        }
        
        for (var z = 0; z < zSize; z++)
        {
            for (var y = 0; y < ySize; y++)
            {
                for (var x = 0; x < xSize; x++)
                {
                    matrix[x, y, z].UpdateSolidState(x, y, z);
                }
            }
        }
    }
}

[Serializable]
public class MeshMatrixTransform : TransformMatrix<MeshVoxel, MeshMatrix>
{
    public bool[] testBitArray = new bool[6];

    public byte testByte;

    public override void Awake()
    {
        MeshVoxel.matrix = matrix;

        base.Awake();
    }
    
    void Update()
    {
        testByte = 0;
        
        for (var i = 0; i < testBitArray.Length; i++)
        {
            if (testBitArray[i]) testByte |= (byte)(1 << i);
        }
    }
    
//    public void OnDrawGizmos()
//    {
//        if (!Application.isPlaying) return;
//
//        for (var z = 0; z < matrix.zSize; z++)
//        {
//            for (var y = 0; y < matrix.ySize; y++)
//            {
//                for (var x = 0; x < matrix.xSize; x++)
//                {
//                    Gizmos.color = matrix.matrix[x, y, z].solid ? Color.black : Color.white;
//                    
//                    Gizmos.DrawCube(new Vector3(x,y,z), Vector3.one * gizmoBlockSize);
//                }
//            }
//        }
//    }

    protected override Color GetGizmoColour(int x, int y, int z)
    {
        return matrix.matrix[x, y, z].solid ? Color.black : Color.white;
    }
}