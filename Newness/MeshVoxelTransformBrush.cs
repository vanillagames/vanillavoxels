﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vanilla.Voxels;

public class MeshVoxelTransformBrush : TransformBrush<MeshVoxel>
{
    public MeshMatrixTransform matrixTransform;
    
    protected override void TransformMoved()
    {
        Paint();
    }

    public void Paint()
    {
        matrixTransform.PaintByWorldPosition(t.position, value);
    }
}
