﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Vanilla.DrawMeshes
{
    [Serializable]
    public class DrawMeshCollection<T> : VanillaBehaviour
        where T : DrawMeshSlotBase
    {
        public List<DrawMeshInstancedSlot> drawMeshSlots = new List<DrawMeshInstancedSlot>();

        public void Awake()
        {
            foreach (var m in drawMeshSlots)
            {
                m.Initialize();
            }
        }

        public void Update()
        {
            foreach (var m in drawMeshSlots)
            {
                m.HandleDraw();
            }

        }
    }
}