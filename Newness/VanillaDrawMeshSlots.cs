﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace Vanilla.DrawMeshes
{
    [Serializable]
    public abstract class DrawMeshSlotBase : VanillaClass
    {
        [SerializeField] public bool active;

        [SerializeField, ReadOnly] private bool drawConditionsValid;

        public Mesh mesh;

        public Material material;

        public int subMeshIndex;


        public bool receiveShadows = true;

        public MaterialPropertyBlock materialPropertyBlock = new MaterialPropertyBlock();

        public LayerMask layer;

        public Camera camera;

        public LightProbeUsage lightProbeUsage;

        public virtual void Initialize()
        {
            UpdateDrawConditionValidity();
        }

        public void UpdateDrawConditionValidity()
        {
            drawConditionsValid = DrawConditionsAreValid();
        }

        public virtual bool DrawConditionsAreValid()
        {
            return mesh && material;
        }

        public void HandleDraw()
        {
            if (active && drawConditionsValid)
            {
                Draw();
            }
        }

        public abstract void Draw();

        public override void Reset()
        {
            mesh = null;
            material = null;
            subMeshIndex = 0;
            receiveShadows = true;
            materialPropertyBlock = new MaterialPropertyBlock();
            layer = LayerMask.NameToLayer("Default");
        }
    }

    public class DrawMeshSlot : DrawMeshSlotBase
    {
        public Vector3 position;

        public Quaternion rotation;

        public Camera camera;
        
        public bool castShadows = true;

        public override bool DrawConditionsAreValid()
        {
            return base.DrawConditionsAreValid();
        }

        public override void Draw()
        {
            Graphics.DrawMesh(
                mesh: mesh,
                position: position,
                rotation: rotation,
                material: material,
                layer: layer,
                camera: camera,
                submeshIndex: subMeshIndex,
                properties: materialPropertyBlock,
                castShadows: castShadows,
                receiveShadows: receiveShadows);
        }

        public override void Reset()
        {
            base.Reset();

            position = Vector3.zero;
            rotation = Quaternion.identity;

            camera = null;

            castShadows = true;
        }
    }

    public abstract class DrawMeshInstancedSlotBase : DrawMeshSlotBase
    {
        public Bounds bounds;
        
        public ComputeBuffer positionBuffer;
        public ComputeBuffer argsBuffer;

        public int argsOffset;

        public ShadowCastingMode shadowCastingMode;
        
        public override void Initialize()
        {
            base.Initialize();
            
            UpdateBuffers();
        }
        
        public override bool DrawConditionsAreValid()
        {
            return base.DrawConditionsAreValid() &&
                   SystemInfo.supportsInstancing &&
                   material.enableInstancing &&
                   positionBuffer != null &&
                   argsBuffer != null;
        }
        
        public abstract override void Draw();

        public abstract void UpdateBuffers();
    }

    public class DrawMeshInstancedSlot : DrawMeshInstancedSlotBase
    {
        public List<Matrix4x4> matrices = new List<Matrix4x4>();

        public void AddMatrix(Vector3 position, Quaternion rotation, Vector3 scale)
        {
            var m = new Matrix4x4();

            m.SetTRS(position, rotation, scale);

            matrices.Add(m);

            UpdateBuffers();
        }

        public override void Draw()
        {
            Graphics.DrawMeshInstanced(
                mesh: mesh, 
                submeshIndex: subMeshIndex, 
                material: material, 
                matrices: matrices,
                properties: materialPropertyBlock,
                castShadows: shadowCastingMode, 
                receiveShadows: receiveShadows, 
                layer: layer, 
                camera: camera, 
                lightProbeUsage: lightProbeUsage);
        }

        public override void UpdateBuffers()
        {
            positionBuffer.Release();
            argsBuffer.Release();

            // The example has 16 for stride but it also sends a Vector4.
            // If stride is the expected byte footprint and we only want to send position, this should be 12?
            // If we want to send a rotation as well, it should be 28?
            positionBuffer = new ComputeBuffer(matrices.Count, 12); 

            positionBuffer.SetData(matrices);
        }
    }

    public class DrawMeshInstancedIndirectSlot : DrawMeshInstancedSlotBase
    {
        public override bool DrawConditionsAreValid()
        {
            return base.DrawConditionsAreValid() &&
                   SystemInfo.supportsComputeShaders;
        }

        public override void Draw()
        {
            Graphics.DrawMeshInstancedIndirect(
                mesh: mesh, 
                submeshIndex: subMeshIndex, 
                material: material, 
                bounds: bounds, 
                bufferWithArgs: argsBuffer, 
                argsOffset: argsOffset, 
                properties: materialPropertyBlock, 
                castShadows: shadowCastingMode, 
                receiveShadows: receiveShadows, 
                layer: layer, 
                camera: camera,
                lightProbeUsage: lightProbeUsage);
        }

        public override void UpdateBuffers()
        {
            
        }
    }
}