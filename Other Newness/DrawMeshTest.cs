﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using Vanilla;
//using Vanilla.Voxels;
//
//public class DrawMeshTest : VanillaBehaviour
//{
//    public ByteTransformMatrix matrix;
//    
//    public const int subMeshIndex = 0;
//
//    public Mesh instanceMesh;
//    public Material instanceMaterial;
//
//    private ComputeBuffer positionBuffer;
//    private ComputeBuffer argsBuffer;
//    private uint[] args = new uint[5] { 0, 0, 0, 0, 0 };
//
//    private List<Vector3> targetPositions = new List<Vector3>();
//    
//    private static readonly int PositionBuffer = Shader.PropertyToID("positionBuffer");
//
//    void Start() {
//        argsBuffer = new ComputeBuffer(1, args.Length * sizeof(uint), ComputeBufferType.IndirectArguments);
//        
//        UpdateBuffers();
//    }
//
//    private void OnEnable()
//    {
//        if (instanceMesh == null)
//        {
//            enabled = false;
//            
//            return;
//        }
//
//        if (!matrix) 
//        {
//            enabled = false;
//
//            return;
//        }
//
//        matrix.matrix.onEdit += HandleMatrixEdit;
//    }
//
//    private void OnDisable()
//    {
//        positionBuffer?.Release();
//
//        positionBuffer = null;
//
//        argsBuffer?.Release();
//
//        argsBuffer = null;
//
//        if (matrix)
//        {
//            matrix.matrix.onEdit -= HandleMatrixEdit;
//        }
//    }
//
//    public void HandleMatrixEdit(int x, int y, int z, byte from, byte to)
//    {
//        UpdateBuffers();
//        
//        // I imagine the way this would work in the future is that a special inherited class for the matrix would
//        // exist that simply overloads its own Set() and handles things there.
//        
//        // Not only that, but I imagine it would directly change the positionBuffer for whatever the respective
//        // block type right there and then and nothing else. For example...
//        
//        // Block XYZ is turned from 0 to 1.
//        // Instead of iterating through the entire chunk, the Set() simply removes XYZ from the 0 DrawMeshHandler
//        // (of which there may be several depending on the block type..?) positionBuffer and adds one instead to the
//        // '1' DrawMeshHandler positionBuffer. Once thats done, it tells the DrawMeshHandler that they've been updated
//        // so they can update the positionBUffers on the GPU. I think thats how this works? The less you send/change/
//        // update the buffer, the better..?
//    }
//
//    void Update() {
//        // Render
//        Graphics.DrawMeshInstancedIndirect(instanceMesh, subMeshIndex, instanceMaterial, new Bounds(Vector3.zero, new Vector3(100.0f, 100.0f, 100.0f)), argsBuffer);
//    }
//
//    void UpdateBuffers() {
//        // Scrape matrix for blocks of 'type' 0
//        targetPositions.Clear();
//
//        for (var z = 0; z < matrix.matrix.zSize; z++)
//        {
//            for (var y = 0; y < matrix.matrix.ySize; y++)
//            {
//                for (var x = 0; x < matrix.matrix.xSize; x++)
//                {
//                    if (matrix.matrix.Get(x, y, z) != 0) continue;
//                    
//                    targetPositions.Add(new Vector3(x,y,z));
//                }
//            }
//        }
//        
//        // Positions
//        positionBuffer?.Release();
//
//        positionBuffer = new ComputeBuffer(targetPositions.Count, 12);
//               
//        positionBuffer.SetData(targetPositions.ToArray());
//        
//        instanceMaterial.SetBuffer(PositionBuffer, positionBuffer);
//
//        args[0] = instanceMesh.GetIndexCount(subMeshIndex);
//        args[1] = (uint)targetPositions.Count;
//        args[2] = instanceMesh.GetIndexStart(subMeshIndex);
//        args[3] = instanceMesh.GetBaseVertex(subMeshIndex);
//
//        argsBuffer.SetData(args);
//    }
//}