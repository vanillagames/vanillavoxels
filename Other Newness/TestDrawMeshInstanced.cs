﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDrawMeshInstanced : MonoBehaviour
{
    public Transform a;
    public Transform b;
    public Transform c;    
    
    public Material grass;
    public Material dirt;
    
    public Mesh cornerOuter;
    public Mesh cornerInner;
    public Mesh cliffEdge;

    public Mesh dirtCornerOuter;
    public Mesh dirtCornerInner;
    public Mesh dirtWall;

//    public List<Vector3> innerCorners = new List<Vector3>();
//    public List<Vector3> outerCorners = new List<Vector3>();
//    public List<Vector3> walls = new List<Vector3>();
    
    public void Update()
    {
//        foreach (Vector3 v in innerCorners)
//        {
            Graphics.DrawMeshInstanced(cornerOuter, 0, grass, new List<Matrix4x4>() {a.localToWorldMatrix}, null);
            Graphics.DrawMeshInstanced(dirtCornerOuter, 0, dirt, new List<Matrix4x4>() {a.localToWorldMatrix}, null);
            
            Graphics.DrawMeshInstanced(cornerInner, 0, grass, new List<Matrix4x4>() {b.localToWorldMatrix}, null);
            Graphics.DrawMeshInstanced(dirtCornerInner, 0, dirt, new List<Matrix4x4>() {b.localToWorldMatrix}, null);

            Graphics.DrawMeshInstanced(cliffEdge, 0, grass, new List<Matrix4x4>() {c.localToWorldMatrix}, null);
            Graphics.DrawMeshInstanced(dirtWall, 0, dirt, new List<Matrix4x4>() {c.localToWorldMatrix}, null);
//        }
        
    }
}