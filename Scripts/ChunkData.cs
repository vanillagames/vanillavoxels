﻿using UnityEngine;

namespace Vanilla.Voxels {

	//[CreateAssetMenu(fileName = "New Chunk Data", menuName = "VanillaVoxels/New Chunk Data")]
	[System.Serializable]
	public class ChunkData : ScriptableObject {

		[SerializeField]
		public BlockData[] blockDatas;

		public void Initialize(int blockDataAmount, int initType) {
			if (blockDatas == null) {
				blockDatas = new BlockData[blockDataAmount];

				for (int i = 0; i < blockDatas.Length; i++) {
					blockDatas[i] = CreateInstance<BlockData>();

					blockDatas[i].Initialize(initType);
				}
			}
		}
	}
}