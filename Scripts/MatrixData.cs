﻿using UnityEngine;

namespace Vanilla.Voxels {

	[CreateAssetMenu(fileName = "New Matrix Data", menuName = "VanillaVoxels/New Matrix Data")]
	[System.Serializable]
	public class MatrixData : ScriptableObject {

		[SerializeField]
		public Vector3Int chunkResolution;

		[SerializeField]
		public Vector3Int blockResolution;

		[SerializeField]
		public float blockSize;

		[SerializeField]
		public int defaultBlockType;

		[SerializeField]
		public ChunkData[] chunkDatas;

		/// <summary>
		/// This should be called on a fresh instance of a MatrixData.
		/// </summary>
		/// <param name="ChunkResolution">How many chunks are featured in the X, Y and Z axes of the Matrix respectively?</param>
		/// <param name="BlockResolution">How many blocks will each chunk contain in the X, Y and Z axes respectively?</param>
		/// <param name="BlockSize">How big should each block be? This uses Unity's world units as a standard measurement.</param>
		/// <param name="newData"></param>
		public void Initialize(Vector3Int ChunkResolution, Vector3Int BlockResolution, float BlockSize, int DefaultBlockType) {
			chunkResolution = ChunkResolution;

			blockResolution = BlockResolution;

			blockSize = BlockSize;

			defaultBlockType = DefaultBlockType;

			if (chunkDatas == null) {
				chunkDatas = new ChunkData[chunkResolution.x * chunkResolution.y * chunkResolution.z];

				int blockDataAmount = blockResolution.x * blockResolution.y * blockResolution.z;

				for (int i = 0; i < chunkDatas.Length; i++) {
					chunkDatas[i] = CreateInstance<ChunkData>();

					chunkDatas[i].Initialize(blockDataAmount, DefaultBlockType);
				}
			}
		}
	}
}