﻿using UnityEngine;

using Vanilla;

namespace Vanilla.Voxels {

	[RequireComponent(typeof(MeshRenderer),typeof(MeshFilter))]
	public class ChunkRenderer : VanillaBehaviour {

		MeshFilter meshFilter;
		MeshRenderer meshRenderer;

		public Chunk chunk;

		public Vector3[,,] localVertPositions;

		// This is the list of each 8 vertex positions that will correspond with each block. The id of this array should match the id of each block.
		public BlockVertSubset[] blockVertPositions;

		public struct BlockVertSubset {
			public int[] positions; // This is an array of ints that point to the 8 index positions of localVertPositions that each block would use
		}

		/// <summary>
		/// The idea here is that a permanent fixed array of possible vert positions for this blockSize is calculated and each block is given an array of the
		/// vert positions it would theoretically use. This would be beneficial because it means all vert-calculating would only happen once technically at the beginning
		/// and there's no reason for verts to double up again either.
		/// </summary>

		public void Initialize(Chunk Chunk) {
			chunk = Chunk;

			meshFilter = GetComponent<MeshFilter>();
			meshRenderer = GetComponent<MeshRenderer>();

			EstablishVertPositions();
			AssignBlockVertSubsets();
		}

		public void EstablishVertPositions() {
			int i = 0;

			localVertPositions = new Vector3[chunk.matrix.data.blockResolution.x + 1, chunk.matrix.data.blockResolution.y + 1, chunk.matrix.data.blockResolution.z + 1];

			for (int z = 0; z < chunk.matrix.data.blockResolution.z+1; z++) {
				for (int y = 0; y < chunk.matrix.data.blockResolution.y+1; y++) {
					for (int x = 0; x < chunk.matrix.data.blockResolution.x+1; x++) {
						localVertPositions[x, y, z] = new Vector3(x, y, z) * chunk.matrix.data.blockSize;

						i++;
					}
				}
			}
		}

		public void AssignBlockVertSubsets() {
			blockVertPositions = new BlockVertSubset[chunk.blocks.Length];

			
		}

		public void OnDrawGizmos() {
			foreach (Vector3 v in localVertPositions) {
				Gizmos.DrawCube(transform.TransformPoint(v), Vector3.one * 0.05f);
			}
		}
	}
}