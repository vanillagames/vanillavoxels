﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

using Vanilla;

namespace Vanilla.Voxels {
	
	public enum BlockType {
		Air = -1, // Also known as 'empty', this block is 'not solid' and also not rendered

		White,
		Grey1,
		Grey2,
		Grey3,
		Grey4,
		Grey5,
		Grey6,
		Black,

		Green1,
		Green2,
		Green3,
		Green4,
		Green5,
		Green6,
		Green7,
		Green8,

		Blue1,
		Blue2,
		Blue3,
		Blue4,
		Blue5,
		Blue6,
		Blue7,
		Blue8,

		Purple1,
		Purple2,
		Purple3,
		Purple4,
		Purple5,
		Purple6,
		Purple7,
		Purple8,

		Red1,
		Red2,
		Red3,
		Red4,
		Red5,
		Red6,
		Red7,
		Red8,

		Orange1,
		Orange2,
		Orange3,
		Orange4,
		Orange5,
		Orange6,
		Orange7,
		Orange8,

		Yellow1,
		Yellow2,
		Yellow3,
		Yellow4,
		Yellow5,
		Yellow6,
		Yellow7,
		Yellow8,

		Misc1,
		Misc2,
		Misc3,
		Misc4,
		Misc5,
		Misc6,
		Misc7,
		Misc8,
	}

	
}