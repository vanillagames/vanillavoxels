﻿using UnityEngine;

namespace Vanilla.Voxels {

	//[CreateAssetMenu(fileName = "New Block Data", menuName = "VanillaVoxels/New Block Data")]
	[System.Serializable]
	public class BlockData : ScriptableObject {

		[SerializeField]
		public int type;

		public void Initialize(int initType) {
			type = initType;
		}
	}
}