﻿using UnityEngine;

using Vanilla;

namespace Vanilla.Voxels {
	[RequireComponent(typeof(Matrix))]
	public class MatrixDataCreator : CommonFunctionEventTrigger {

		public Matrix matrix;

		[Header("New Matrix Properties")]
		public Vector3Int chunkResolution = new Vector3Int(4, 4, 4);
		public Vector3Int blockResolution = new Vector3Int(4, 4, 4);

		public float blockSize = 0.25f;

		public BlockType defaultBlockType;

		public override void LoadConditionsMet() {
			if (!matrix) {
				matrix = GetComponent<Matrix>();
			}

			matrix.CreateNewMatrixData(chunkResolution, blockResolution, blockSize, defaultBlockType);
		}
	}
}
