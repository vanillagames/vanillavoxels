﻿using UnityEngine;

//using System;
//using System.Collections;
//using System.Collections.Generic;

using Vanilla;
using Vanilla.Math;

namespace Vanilla.Voxels {

	[System.Serializable]
	public class Chunk : VanillaBehaviour {

		public Matrix matrix;

		public Vector3Int positionWithinMatrix;

		public int chunkID;

		public Block[,,] blocks;

		public Vector3[] vertPositions;

		public void Initialize(Matrix Matrix, Vector3Int PositionWithinMatrix, Vector3Int BlockResolution, int ChunkID, float BlockSize) {
			matrix = Matrix;

			chunkID = ChunkID;

			positionWithinMatrix = PositionWithinMatrix;

			vertPositions = new Vector3[(BlockResolution.x + 1) * (BlockResolution.y + 1) * (BlockResolution.z + 1)];

			#region Establish Vertex Positions
			int i = 0;

			for (int z = 0; z < BlockResolution.z + 1; z++) {
				for (int y = 0; y < BlockResolution.y + 1; y++) {
					for (int x = 0; x < BlockResolution.x + 1; x++) {
						vertPositions[i] = new Vector3(x, y, z) * BlockSize;

						//blocks[(x < BlockResolution.x ? x : x-1), (x < BlockResolution.x ? x : x - 1), (x < BlockResolution.x ? x : x - 1)].vertPositions[]

						// I'm thinking that the Block vert positions could be established somewhere around here, since you could think about it a bit more laterally
						// with the help of a 3D array/for-loop (and the index iterator 'i' as well) all in the one place.
						// Where it gets weird is that you need some sort of pre-established order for the block vertPositions array, like:

						// 0 = left bottom back (-x,-y,-z)
						// 1 = right bottom back (x,-y,-z)
						// 2 = left top back (-x, y, -z)
						// 3 = right top back (x, y, -z)
						// ...and so on...

						// But figuring out how to assign those correctly is gonna get really weird, especially if the block resolutions aren't uniform.
						// Just use your good old trusty dusty method of drawing out the correct answer as a cheat-sheet and then figuring out what the formula is
						// that way. You'll need to literally draw it all out, so maybe just make them all text objects and cubes in Unity so its easy to navigate?

						i++;
					}
				}
			}
			#endregion

			#region Create Block Instances
			blocks = new Block[BlockResolution.x, BlockResolution.y, BlockResolution.z];

			i = 0;

			for (int z = 0; z < BlockResolution.z; z++) {
				for (int y = 0; y < BlockResolution.y; y++) {
					for (int x = 0; x < BlockResolution.x; x++) {
						blocks[x, y, z] = new Block(matrix.data.chunkDatas[chunkID].blockDatas[i].type, i);
						blocks[x, y, z].vertPositions = new int[8];

						//blocks[x, y, z].vertPositions[0] = 
						i++;
					}
				}
			}
			#endregion
		}

		public void OnDrawGizmos() {
			if (!matrix.showVertsWithGizmos) {
				return;
			}

			foreach (Vector3 v in vertPositions) {
				Gizmos.DrawCube(transform.TransformPoint(v), Vector3.one * 0.05f);
			}
		}
	}
}