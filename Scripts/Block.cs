﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

using Vanilla;

namespace Vanilla.Voxels {
	public class Block {

		public BlockData data;

		public BlockType indexWithinChunk;

		public int[] vertPositions;

		public Block(int initType, int IndexWithinChunk) {
			indexWithinChunk = (BlockType)IndexWithinChunk;
		}
	}
}