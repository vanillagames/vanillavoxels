﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

namespace Vanilla.Voxels {

	#region Editor
#if UNITY_EDITOR
	using UnityEditor;

	[CustomEditor(typeof(Matrix))]
	public class MatrixEditor : VanillaEditor {

		Matrix matrix;

		public void OnEnable() {
			matrix = (Matrix)target;
		}

		public override void OnInspectorGUI() {
			InspectorGUIBegin();

			if (Application.isPlaying) {
				if (GUILayout.Button("Create Chunk Objects")) {
					matrix.CreateChunkObjects();
				}
			}

			InspectorGUIEnd();
		}
	}
#endif
	#endregion

	public class Matrix : VanillaBehaviour{

		public MatrixData data;

		public bool renderMatrix = true;
		public bool showVertsWithGizmos = true;

		public Chunk[,,] chunks;

		// This should be called first.
		// If the data field isn't populated at the time, it will create a fresh MatrixData using the current properties.
		// If it is, the matrix properties will simply be copied over.
		public void LoadMatrixData(MatrixData Data) {
			data = Data;
		}

		public void CreateNewMatrixData(Vector3Int ChunkResolution, Vector3Int BlockResolution, float BlockSize, BlockType DefaultBlockType) {
			data = ScriptableObject.CreateInstance<MatrixData>();
			data.Initialize(ChunkResolution, BlockResolution, BlockSize, (int)DefaultBlockType);
		}

		// This is expected to be called only after all parts of the voxel data tree (Matrix -> Chunks -> Blocks) have
		// been created or populated in some way. This is handled by running LoadFromData first.
		public void CreateChunkObjects() {
			if (!data) {
				Error("We can't proceed without a MatrixData asset to read from. Try using a MatrixDataCreator or a MatrixDataLoader component first to fulfill this.");
			}

			chunks = new Chunk[data.chunkResolution.x, data.chunkResolution.y, data.chunkResolution.z];

			int i = 0;

			for (int z = 0; z < data.chunkResolution.z; z++) {
				for (int y = 0; y < data.chunkResolution.y; y++) {
					for (int x = 0; x < data.chunkResolution.x; x++) {
						GameObject chunkObject = new GameObject("Chunk[" + i + "]");

						chunkObject.transform.SetParent(transform, false);
						chunkObject.transform.localPosition = new Vector3(x * data.chunkResolution.x, y * data.chunkResolution.y, z * data.chunkResolution.z) * data.blockSize;

						chunks[x, y, z] = chunkObject.AddComponent<Chunk>();

						chunks[x, y, z].Initialize(this, new Vector3Int(x, y, z), data.blockResolution, i, data.blockSize);

						// I get the feeling there will be a better way to do this bit.
						if (renderMatrix) {
							ChunkRenderer newChunkRenderer = chunks[x, y, z].gameObject.AddComponent<ChunkRenderer>();
							newChunkRenderer.Initialize(chunks[x, y, z]);
						}

						i++;
					}
				}
			}
		}

		public int XYZToArrayIndex(int x, int y, int z, int xResolution, int yResolution) {
			return (z * xResolution * yResolution) + (y * xResolution) + x;
		}

		public void ArrayIndexToXYZ(int arrayIndex, int xResolution, int yResolution, out int x, out int y, out int z) {
			z = arrayIndex / (xResolution * yResolution);

			arrayIndex -= (z * xResolution * yResolution);

			y = arrayIndex / xResolution;
			x = arrayIndex % xResolution;
		}
	}
}