﻿using UnityEngine;

using Vanilla;

namespace Vanilla.Voxels {
	[RequireComponent(typeof(Matrix))]
	public class MatrixDataLoader : CommonFunctionEventTrigger {

		public Matrix matrix;

		[Header("MatrixData Asset File")]
		public MatrixData data;

		public override void LoadConditionsMet() {
			if (!matrix) {
				matrix = GetComponent<Matrix>();
			}

			matrix.LoadMatrixData(data);
		}
	}
}